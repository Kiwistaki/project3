/*
 * Simple démonstration audio avec plb_ac97.
 *
 * Auteur : Philippe Proulx <philippe.proulx@polymtl.ca>
 * Date : 25 août 2011
 *
 * Ce fichier se lit mieux avec une tabulation équivalente à 8 espaces.
 */
#include <xbasic_types.h>
#include <xparameters.h>
#include <xio.h>

#include "xac97_l.h"

#define PLB_AC97_BASEADDR	XPAR_PLB_AC97_0_BASEADDR
#define NUM_SAMPLES		10301
#define STRETCH			5

extern Xint16 g_samples [];

int main(void) {
	u32 stretch;
	u32 samp_i;

	/* Initialisation de plb_ac97 : */
	XAC97_HardReset(PLB_AC97_BASEADDR);
	XAC97_InitAudio(PLB_AC97_BASEADDR, 0);
	XAC97_ClearFifos(PLB_AC97_BASEADDR);

	/* Ajustement du volume audio : */
	XAC97_WriteReg(PLB_AC97_BASEADDR, AC97_MasterVol, AC97_VOL_MAX);
	XAC97_WriteReg(PLB_AC97_BASEADDR, AC97_AuxOutVol, AC97_VOL_ATTN_6_0_DB);
	XAC97_WriteReg(PLB_AC97_BASEADDR, AC97_PCMOutVol, AC97_VOL_ATTN_6_0_DB);

	/* Boucler : */
	for (;;) {
		for (samp_i = 0; samp_i < NUM_SAMPLES; ++samp_i) {
			for (stretch = 0; stretch < STRETCH; ++stretch) {
				XAC97_WriteFifo(PLB_AC97_BASEADDR, g_samples[samp_i]);
			}
		}
	}

	return 0;
}
